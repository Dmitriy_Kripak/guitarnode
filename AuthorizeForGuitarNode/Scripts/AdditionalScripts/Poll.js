﻿var i = 0;
var intervalId = window.setInterval(function () {
    // send AJAX requests every 'x' seconds to check for updates
    $.getJSON('@Url.Action("poll")', function (dataFromServer) {
        document.getElementById('RandomPhrase').innerHTML = dataFromServer.Content;
        document.getElementById('PhraseAuthor').innerHTML = dataFromServer.PhraseAuthor;
    });
}, 4000);