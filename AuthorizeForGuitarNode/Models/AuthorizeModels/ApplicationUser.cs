﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace AuthorizeForGuitarNode.Models
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        public string RegisterDate { get; set; }

        public ApplicationUser()
        {

        }
    }
}