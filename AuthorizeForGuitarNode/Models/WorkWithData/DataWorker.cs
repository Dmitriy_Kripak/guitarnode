﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AuthorizeForGuitarNode.Models.Articles;
using AuthorizeForGuitarNode.Models.Extentions;
using System.IO;
using System.Data.Entity;

namespace AuthorizeForGuitarNode.Models.WorkWithData
{
    public class DataWorker
    {
        List<Comment> allComments;
        StreamWriter fileWriter;
        StreamReader fileReder;
        ArticlesContext ctxt;
        Comment comment;
        Article article;
        Phrase phrase;

        public DataWorker()
        {
            //fileWriter = new StreamWriter(HttpContext.Current.Server.MapPath(@"App_Data\ErrorLog\DataErrors.txt"));
            //fileWriter = new StreamWriter(@"C:\Users\Dmitriy\Desktop\AuthorizeForGuitarNode\AuthorizeForGuitarNode\App_Data\ArticlesCtxt.mdf");
            //fileWriter = new StreamWriter(HttpContext.Current.Server.MapPath("/App_Data/ArticlesCtxt.mdf"));
            //ctxt = new ArticlesContext();
        }

        public List<Comment> GetAllComments(string articleId)
        {
            using (ctxt = new ArticlesContext())
            {
                try
                {
                    allComments = new List<Comment>(
                         ctxt.Comments.Where(com => com.ArticleId == articleId).
                         OrderByDescending(com => com.DateOfIssue)
                        );
                }
                catch (Exception ex)
                {
                    using (fileWriter = new StreamWriter(HttpContext.Current.Server.MapPath("/App_Data/ArticlesCtxt.mdf")))
                    {
                        fileWriter.WriteLine(string.Format("{0},   {1},   {2}", DateTime.Now, ex.TargetSite, ex.Message));
                    }
                }
            }
            return allComments;
        }

        public Comment AddNewComment(string commentContent, string articleId)
        {
            using (ctxt = new ArticlesContext())
            {
                //Предусмотреть если параметры null
                try
                {
                    comment = new Comment()
                    {
                        CommentId = Guid.NewGuid().ToString(),
                        CommentLink = commentContent,
                        UserId = Guid.NewGuid().ToString(),
                        ArticleId = articleId,
                        DateOfIssue = DateTime.Now
                    };
                    article = ctxt.Articles.Where(c => c.ArticleId == comment.ArticleId).FirstOrDefault();
                    //CommentCount increment
                    article.CommentCount += 1;
                    ctxt.Comments.Add(comment);
                    ctxt.SaveChanges();
                    return comment;
                }
                catch (Exception ex)
                {
                    using (fileWriter = new StreamWriter(HttpContext.Current.Server.MapPath("/App_Data/ArticlesCtxt.mdf")))
                    {
                        fileWriter.WriteLine(string.Format("{0},   {1},   {2}", DateTime.Now, ex.TargetSite, ex.Message));
                    }
                    return null;
                }
            }
        }

        public Phrase GetRandomPhrase()
        {
            using (ArticlesContext context = new ArticlesContext())
            {
                phrase = context.Phrases.Random();
            }

            return phrase;
        }

        public void AddArticle()
        {
            fileReder = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/Article1/Description.txt"));
            using (ctxt = new ArticlesContext())
            {
                ctxt.Articles.Add(new Article
                {
                    ArticleId = Guid.NewGuid().ToString(),
                    Content = fileReder.ReadToEnd(),
                    DateOfIssue = DateTime.Now,
                });
                ctxt.SaveChanges();
            }
        }

        public void AddInitPhrase()
        {
            using (ctxt = new ArticlesContext())
            {
                //Предусмотреть если параметры null
                try
                {
                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Я спокойно покину планету, потому, что знаю - после меня останется моя музыка",
                        PhraseAuthor = "Алекс Ван Хален",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/AlexVanHalen.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Не знаю, что исчезнет сначала, религия или рок... Делаю ставки на религию...",
                        PhraseAuthor = "Джон Леннон, Beatles",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/JohnLennon.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Не обязательно верить в себя, но абсолютно обязательно верить в то, что ты делаешь!",
                        PhraseAuthor = "Джимми Пейдж, Led Zeppelin",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/JimmyPage.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Никогда не бывает такого, чтобы я остался чем-то доволен. Я постоянно стараюсь совершенствовать свою музыку",
                        PhraseAuthor = "Ричи Блэкмор, Deep Pupple",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/RitchieBlackmore.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Единственная сила, которая толкает нас вперед – это музыка, а совсем не желание прославиться",
                        PhraseAuthor = "Роджер Уотерс, Pink Floyd",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/RogerWaters.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Никаких концепций – только собственные впечатления, эмоции и опыт – вот что такое рок!",
                        PhraseAuthor = "Йен Гиллан, Black Sabbath",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/IenGillan.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Рок играют не по нотам, а по интуиции",
                        PhraseAuthor = "Брайан Мэй, Queen",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/BrayanMay.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Первым инструментом, который изобрели люди, был барабан. Потому, что люди хотели как-то воспроизвести первый звук, который они услышали в жизни – звук сердца матери",
                        PhraseAuthor = "Рик Сэведж, Def Leppard",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/RickSavage.jpeg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Все эти разговоры о металле и харде… я не подпишусь ни под единым словом. Рок – это просто хорошая музыка. Его нужно слушать, а не говорить о нем!",
                        PhraseAuthor = "Оззи Осборн, Black Sabbath",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/OzzyOsbourne.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Я никогда никого не осуждаю. Моя любимая индийская пословица: «Не суди другого, пока не прошел в его мокасинах две мили",
                        PhraseAuthor = "Эрик Адамс, Manowar",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/EerikAdamsManowar.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Кто-то говорил мне недавно – умирать тяжело и страшно. Вранье! Умирать – это легко. Вот что действительно трудно – так это жить",
                        PhraseAuthor = "Джеймс Хэтфилд, Metallica",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/JamesHetfield2012.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Если ваше окружение не позволяет вам мечтать, смените его немедленно!",
                        PhraseAuthor = "Билли Айдол, Generation X",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/BillyIdol.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "В современном мире очень тяжело поверить в свет в конце тоннеля. Слава богу, у нас есть старый добрый рок!",
                        PhraseAuthor = "Маттиас Ябс, Scorpions",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/MatiasJabs.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Для каждого мужчины движущая сила – это женщина. Без женщины даже Наполеон был бы простым идиотом",
                        PhraseAuthor = "Джон Леннон, Beatles",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/JohnLennon.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Лучше умереть на обочине, но знать, что ты сделал всё, чтобы достичь цели. Чем прожить 100 лет в достатке, осознавая, что изменил себе",
                        PhraseAuthor = "Роберт Плант Led Zeppelin",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/RobertPlant.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Продолжайте делать то, что делаете. Прикидывайтесь, что вы это делаете, и так до тех пор, пока дело не будет сделано",
                        PhraseAuthor = "Стивен Тайлер, Aerosmith",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/Taller.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Жизнь – это путешествие, а не пункт назначения",
                        PhraseAuthor = "Стивен Тайлер, Aerosmith",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/Taller.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Музыка – это музыка. И, если она была создана с чувством, то не заметить ее просто невозможно, сколько бы лет тебе ни было",
                        PhraseAuthor = "Дэниел Маккаферти,  Nazareth",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/DanielMcCaferty.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Бывают моменты, когда ничего не осветит ваш путь лучше горящего моста",
                        PhraseAuthor = "Дон Хенли, Eagles",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/DonHaley.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Когда я понял, что рок есть везде? Это, когда люди, которые путешествовали по Амазонке, рассказали мне, что дети туземцев стирали белье в реке и напевали Wind Of Change",
                        PhraseAuthor = "Клаус Майне, Scorpions",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/ClausMine.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Человек рождается не ради музыки единой. Поэтому никогда не воспринимайте музыку слишком серьезно. Пусть она развлекает вас",
                        PhraseAuthor = "Ричи Блэкмор, Deep Pupple | Rainbow",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/RitchieBlackmore.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    phrase = new Phrase()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Content = "Вокруг тебя всегда будут крутиться как негативно настроенные, так и позитивные люди. Просто будь собой и делай всё, что в твоих силах. И плевать на мнение окружающих!",
                        PhraseAuthor = "Джоуи ДиМайо, Manowar",
                        AuthorsPortrait_Link = "~/Resources/PhraseAuthorsPortrait/JouiDimajo.jpg"
                    };
                    ctxt.Phrases.Add(phrase);

                    ctxt.SaveChanges();
                }
                catch (Exception ex)
                {
                    using (fileWriter = new StreamWriter(HttpContext.Current.Server.MapPath("/App_Data/ArticlesCtxt.mdf")))
                    {
                        fileWriter.WriteLine(string.Format("{0},   {1},   {2}", DateTime.Now, ex.TargetSite, ex.Message));
                    }
                }
            }
        }

        public void DeleteDuplicate()
        {
            using (ArticlesContext context = new ArticlesContext())
            {
                //Получаем различающиеся элементы из таблицы
                IEnumerable<Phrase> phraseUniq = context.Phrases.Distinct<Phrase>();
                //Удаляем все данные из таблицы
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Phrases]");
                //context.SaveChanges();
                //Добавляем коллекцию уникальных
                context.Phrases.AddRange(phraseUniq);
                context.SaveChanges();
            }
        }

    }
}

