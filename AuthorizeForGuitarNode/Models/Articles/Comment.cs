﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorizeForGuitarNode.Models.Articles
{
    public class Comment
    {
        public string CommentId { get; set; }

        public string CommentLink { get; set; }

        public string UserId { get; set; }

        public string ArticleId { get; set; }

        public DateTime DateOfIssue { get; set; }

    }
}