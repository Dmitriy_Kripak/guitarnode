﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using System.IO;

namespace AuthorizeForGuitarNode.Models.Articles
{
    //Data Context
    public class ArticlesContext : DbContext
    {
        static ArticlesContext()
        {
            Database.SetInitializer<ArticlesContext>(new ContextInit());
        }

        public ArticlesContext() : base("ArticlesCtxt")
        {

        }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Phrase> Phrases { get; set; }

    }

    //Context Initializer
    public class ContextInit : CreateDatabaseIfNotExists<ArticlesContext>// DropCreateDatabaseAlways<ArticlesContext>//
    {
        protected override void Seed(ArticlesContext context)
        {
            try
            {
                StreamReader readDescription, readContent;

                string IdForArticle1 = Guid.NewGuid().ToString();
                string IdForArticle2 = Guid.NewGuid().ToString();
                string IdForPhoto = Guid.NewGuid().ToString();
                //string IdForUser = Guid.NewGuid().ToString();
                //string IdForComment = Guid.NewGuid().ToString();

                #region InitialData
                readDescription = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/Article1/Description.txt"));
                readContent = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/Article1/GuitarCurriculum.html"));
                Article art1 = new Article
                {
                    ArticleId = IdForArticle1,
                    Name = "GuitarCurriculum",
                    Description = readDescription.ReadToEnd(),
                    Content = readContent.ReadToEnd(),
                    Viewed = 0,
                    CommentCount = 0,
                    DateOfIssue = DateTime.Now
                };

                readDescription = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/Article2/Description.txt"));
                readContent = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/Article2/GuitarLessons.html"));
                Article art2 = new Article
                {
                    ArticleId = IdForArticle2,
                    Name = "GuitarLessons",
                    Description = readDescription.ReadToEnd(),
                    Content = readContent.ReadToEnd(),
                    Viewed = 0,
                    CommentCount = 0,
                    DateOfIssue = DateTime.Now
                };

                Photo photo1 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle1,
                    PhotoLinlk = "/Resources/Article1/1.jpg"
                };

                Photo photo2 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle1,
                    PhotoLinlk = "/Resources/Article1/2.jpg"
                };

                Photo photo3 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle2,
                    PhotoLinlk = "/Resources/Article2/1.jpg"
                };

                Photo photo4 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle2,
                    PhotoLinlk = "/Resources/Article2/2.jpg"
                };

                #endregion

                context.Articles.Add(art1);
                context.Articles.Add(art2);
                context.Photos.Add(photo1);
                context.Photos.Add(photo2);
                context.Photos.Add(photo3);
                context.Photos.Add(photo4);

                context.SaveChanges();

                base.Seed(context);
                throw new Exception();
            }
            catch(Exception ex)
            {
                File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/ErrorLog/DataErrors.txt"), ex.Message);
            }
        }
    }
}