﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorizeForGuitarNode.Models.Articles
{
    public class Phrase
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public string PhraseAuthor { get; set; }

        public string AuthorsPortrait_Link { get; set; }

    }
}