﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.IO;



namespace AuthorizeForGuitarNode.Models.Articles
{
    public class Article
    {
        public string ArticleId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Content { get; set; }
       
        public int? Viewed { get; set; }

        public int? CommentCount { get; set; }

        public DateTime DateOfIssue { get; set; }

        public void SomeMeth()
        {
            try
            {
                throw new Exception();
            }
            catch (Exception ex)
            {
                File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/ErrorLog/DataErrors.txt"), string.Format(ex.Message + " " + DateTime.Now ));
            }
        
        }
    }
}