﻿///данный файл организует связь между OWIN и приложением. 
///OWIN представляет спецификацию, описывающую взаимодействие между компонентами приложения
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Microsoft.Owin;
using Owin;
using AuthorizeForGuitarNode.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(AuthorizeForGuitarNode.App_Start.Startup))]

namespace AuthorizeForGuitarNode.App_Start
{
    /// <summary>
    /// Интерфейс IAppBuilder :
    /// CreatePerOwinContext - регистрирует в OWIN менеджер пользователей ApplicationUserManager и класс контекста ApplicationContext;
    /// UseCookieAuthentication - с помощью объекта CookieAuthenticationOptions устанавливает аутентификацию на основе куки в качестве
    /// типа аутентификации в приложении.А свойство LoginPath позволяет установить адрес URL, по которому будут перенаправляться 
    /// неавторизованные пользователи.
    /// </summary>
    
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // настраиваем контекст, менеджер пользователей и ролей
            app.CreatePerOwinContext<ApplicationContext>(ApplicationContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                //Включает переадресацию, в случае [Authorization]
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive

            });
        }
    }

}