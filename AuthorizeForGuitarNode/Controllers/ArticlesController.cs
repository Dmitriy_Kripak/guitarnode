﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AuthorizeForGuitarNode.Models.Articles;
using AuthorizeForGuitarNode.Models.WorkWithData;
using PagedList;
using PagedList.Mvc;
using AuthorizeForGuitarNode.Helpers;

namespace AuthorizeForGuitarNode.Controllers
{

    public class ArticlesController : Controller
    {
        List<Article> articleCollection;
        Article article;
        DataWorker dataWorker;
        Comment cmt;
        int currentPage, pageQuantityObjects = 8;// количество объектов на страницу;

        // GET: Home
        public ActionResult IndexArticles(int? page)
        {
            ArticlesContext ctxt = new ArticlesContext();
            currentPage = (page ?? 1);
            articleCollection = new List<Article>(ctxt.Articles);
            ctxt.Dispose();
            IEnumerable<Article> phonesPerPages = articleCollection.Skip((page ?? 1 - 1) * pageQuantityObjects).Take(pageQuantityObjects);
            PageInfo pageInfo = new PageInfo { PageNumber = page ?? 1, PageSize = pageQuantityObjects, TotalItems = articleCollection.Count };
            IndexViewModel ivm = new IndexViewModel { PageInfo = pageInfo, Articles = phonesPerPages };

            return View(ivm);
        }

        [HttpPost]
        public ActionResult ArticleViewer(string id)
        {
            if (id != null)
            {
                using (ArticlesContext ctxt = new ArticlesContext())
                {
                    try
                    {
                        article = new Article();
                        article = ctxt.Articles.Where(c => c.ArticleId == id).FirstOrDefault();
                        //View count
                        article.Viewed += 1;
                        Session["currentArticle"] = article.ArticleId;
                        ctxt.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        return Content(ex.Message);
                    }
                }
            }
            else
                return Content("Something wrong!!!");

            return View(article);
        }

        //Добавляем комментарий
        [HttpPost]
        [Authorize]
        public ActionResult ContentComment(string commentContent)
        {
            dataWorker = new DataWorker();
            string articleId = Session["currentArticle"].ToString();

            if (commentContent != null && commentContent != string.Empty)
               cmt = dataWorker.AddNewComment(commentContent, articleId);
            return View(cmt);
        }

        public ActionResult CommentViewer()
        {
            dataWorker = new DataWorker();
            string articleId = Session["currentArticle"].ToString();

            return View(dataWorker.GetAllComments(articleId));
        }
    }
}