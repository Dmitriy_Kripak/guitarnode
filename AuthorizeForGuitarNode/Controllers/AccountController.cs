﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AuthorizeForGuitarNode.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.IO;
using AuthorizeForGuitarNode.CustomAttributes;

namespace AuthorizeForGuitarNode.Controllers
{
    public class AccountController : Controller
    {
        #region Properties
        ///свойство UserManager, возвращающее объект ApplicationUserManager.Через него мы будем взаимодействовать с хранилищем пользователей.
        ///Для получения хранилища применяется выражение HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>()
        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        /// <summary>
        /// Свойство, управляющее входом на сайт
        /// Интерфейс IAuthenticationManager :
        /// SignIn(): создает аутентификационные куки
        /// SignOut(): удаляет аутентификационные куки
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion

        #region Методы регистрации
        public ActionResult Register()
        {
            TempData["referrer"] = HttpContext.Request.UrlReferrer;
            return View();
        }

        /// <summary>
        ///  представляет асинхронный метод, поскольку для создания пользователя здесь используется асинхронный вызов UserManager.CreateAsync().
        ///  Этот метод возвращает объект IdentityResult.Если создание пользователя прошло успешно, то его свойство Succeeded будет равно true.
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser { UserName = model.Email, Email = model.Email, RegisterDate = DateTime.Now.ToString() };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "user");

                    if (TempData["referrer"] != null)
                    {
                        // return RedirectToRoute(Session["referrer"].ToString());
                        return Redirect(TempData["referrer"].ToString());
                       
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model);
        }

        #endregion

        #region Методы входа на сайт

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <variable name="claim"> Тип "ClaimsIdentity", который представляет реализацию интерфейса IIdentity. Айтентифик. происходит на основе этого объекта</variable>
        /// <method name="AuthenticationManager.SignIn()">Осуществляет вход, ранее зарегистрированного пользователя
        /// <param name="IsPersistent">Позволяет сохранять аутентификационные данные в браузере после его закрытия</param>
        /// </method>
        /// <method name="Logout()">Удаляет аутентификационные куки в браузере</method>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);

                    AuthenticationManager.SignOut();

                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    },
                                                claim);
                    // Добавляем роль по умолчанию
                    //string Id = User.Identity.GetUserId();

                    //StreamWriter sw = new StreamWriter(Server.MapPath("~/App_Data/ErrorLog.txt"), true);
                    //sw.WriteLine(Id);
                    //sw.Dispose();

                    //UserManager.AddToRole(user.Id, "user");

                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Home");

                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }
        #endregion
    }
}