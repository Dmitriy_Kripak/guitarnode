﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AuthorizeForGuitarNode.Models.Articles;
using AuthorizeForGuitarNode.Models.WorkWithData;
using System.IO;

namespace AuthorizeForGuitarNode.Controllers
{
    public class HomeController : Controller
    {
        Phrase phrase;
        Article art;
        DataWorker dataWorker;
        // GET: Home
        public ActionResult Index()
        {
            //dataWorker = new DataWorker();
            //dataWorker.DeleteDuplicate();
            //dataWorker.AddInitPhrase();
            //Добавляем на страницу первую рандомную фразу
            phrase = new DataWorker().GetRandomPhrase();
            TempData["randomContent"] = phrase.Content; 
            TempData["randomAuthor"] = phrase.PhraseAuthor;
            TempData["randomPictureAuthor"] = phrase.AuthorsPortrait_Link;
            //____________________________________________
            return View();
        }

        [HttpPost]
        public ActionResult Index(string value)
        {
            return View();
        }

        public void InitialData(ArticlesContext context, int countAddingData)
        {
            context.Configuration.AutoDetectChangesEnabled = false;
            StreamReader readDescription, readContent;

            for (int i = 0; i < countAddingData; i++)
            {
                string IdForArticle1 = Guid.NewGuid().ToString();
                string IdForArticle2 = Guid.NewGuid().ToString();

                #region InitialData
                readDescription = new StreamReader(Server.MapPath("~/Resources/Article1/Description.txt"));
                readContent = new StreamReader(Server.MapPath("~/Resources/Article1/GuitarCurriculum.html"));
                Article art1 = new Article
                {
                    ArticleId = IdForArticle1,
                    Name = "GuitarCurriculum",
                    Description = readDescription.ReadToEnd(),
                    Content = readContent.ReadToEnd(),
                    Viewed = 0,
                    CommentCount = 0,
                    DateOfIssue = DateTime.Now
                };

                readDescription = new StreamReader(Server.MapPath("~/Resources/Article2/Description.txt"));
                readContent = new StreamReader(Server.MapPath("~/Resources/Article2/GuitarLessons.html"));
                Article art2 = new Article
                {
                    ArticleId = IdForArticle2,
                    Name = "GuitarLessons",
                    Description = readDescription.ReadToEnd(),
                    Content = readContent.ReadToEnd(),
                    Viewed = 0,
                    CommentCount = 0,
                    DateOfIssue = DateTime.Now
                };

                Photo photo1 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle1,
                    PhotoLinlk = "/Resources/Article1/1.jpg"
                };

                Photo photo2 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle1,
                    PhotoLinlk = "/Resources/Article1/2.jpg"
                };

                Photo photo3 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle2,
                    PhotoLinlk = "/Resources/Article2/1.jpg"
                };

                Photo photo4 = new Photo
                {
                    PhotoId = Guid.NewGuid().ToString(),
                    ArticleId = IdForArticle2,
                    PhotoLinlk = "/Resources/Article2/2.jpg"
                };

                #endregion

                context.Articles.Add(art1);
                context.Articles.Add(art2);
                context.Photos.Add(photo1);
                context.Photos.Add(photo2);
                context.Photos.Add(photo3);
                context.Photos.Add(photo4);

            }
            context.ChangeTracker.DetectChanges();
            context.SaveChanges();

        }

        public void InitailPhrases(ArticlesContext context, int countAddingData)
        {
            context.Configuration.AutoDetectChangesEnabled = false;

            for (int i = 0; i < countAddingData; i++)
            {
                context.Phrases.Add(new Phrase
                {
                    Id = Guid.NewGuid().ToString(),
                    Content = string.Format("Phrase {0}", i),
                    PhraseAuthor = string.Format("PhraseAuthor {0}", i)
                }
                                    );
            }
            context.ChangeTracker.DetectChanges();
            context.SaveChanges();
        }

        public ActionResult Poll()
        {
            phrase = new DataWorker().GetRandomPhrase();
            return Json(phrase, JsonRequestBehavior.AllowGet);
        }

    }
}